"use strict";

document.addEventListener('DOMContentLoaded', function(){
    const arrowLeft = document.querySelector(".arrow-left");
        const arrowRight = document.querySelector(".arrow-right");
        const profileList = document.querySelector(".profile_list");
        const profiles = profileList.querySelectorAll("li");
        const profileImg = document.querySelectorAll('.photo_nav')

        let currentProfileIndex = 0;

        arrowLeft.addEventListener("click", function() {
            profiles[currentProfileIndex].classList.remove("active");
            profileImg[currentProfileIndex].classList.remove("active");
            currentProfileIndex = (currentProfileIndex - 1 + profiles.length) % profiles.length;
            profiles[currentProfileIndex].classList.add("active");
            profileImg[currentProfileIndex].classList.add("active");

        });

        arrowRight.addEventListener("click", function() {
            profiles[currentProfileIndex].classList.remove("active");
            profileImg[currentProfileIndex].classList.remove("active");
            currentProfileIndex = (currentProfileIndex + 1) % profiles.length;
            profiles[currentProfileIndex].classList.add("active");
            profileImg[currentProfileIndex].classList.add("active");

        });

        profiles[currentProfileIndex].classList.add("active");
        profileImg[currentProfileIndex].classList.add("active");
        
});