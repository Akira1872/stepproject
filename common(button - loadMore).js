"use strict";

document.addEventListener('DOMContentLoaded', function(){
    const galleryToggleButton = document.querySelector('.gallery_button');
const loadingAnimationGallery = document.querySelector('#loadingAnimationGallery');
let isGalleryVisible = true;

galleryToggleButton.addEventListener("click", function () {
    isGalleryVisible = !isGalleryVisible;
  
    
    galleryToggleButton.style.display = "none";
    loadingAnimationGallery.style.display = "block";
    setTimeout(() => {
        galleryToggleButton.style.display = "block";
    },3000);

    setTimeout(() => {
        loadingAnimationGallery.style.display = "none";
    }, 3000);

});

})