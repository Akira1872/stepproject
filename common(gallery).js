"use strict";

document.addEventListener('DOMContentLoaded', function(){
    function setupMasonry(containerSelector) {
        const container = document.querySelector(containerSelector);
        const masonry = new Masonry(container, {
            itemSelector: '.gallery_img',
            columnWidth: '.gallery_img',
        });
    
        window.addEventListener('resize', function () {
            masonry.layout();
        });
    }
    
    window.addEventListener('load', function () {
        setupMasonry('.gallery_list1');
        setupMasonry('.gallery_list3');
        setupMasonry('.gallery_list4');
        setupMasonry('.gallery_list5');
    
    });
});