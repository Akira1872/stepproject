"use strict";

document.addEventListener("DOMContentLoaded", function () {
const tabs = document.querySelectorAll('.active_tab');
const ourService = document.querySelectorAll('.our_services_nav_content_box');
let activeTab = null;

const hideContentService = () => {
    ourService.forEach(e => {
        e.style.display = 'none';
    });
};

const showDefaultTab = () => {
    const defaultTab = tabs[0];
    defaultTab.classList.add('active');
    defaultTab.style.color = 'white'
    const defaultTabName = defaultTab.getAttribute('data-tab');
    const defaultContentItem = document.querySelector(`.our_services_nav_content_box[data-tab="${defaultTabName}"]`);
    defaultContentItem.style.display = 'block';
    activeTab = defaultTab;
};

hideContentService();
showDefaultTab();

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        if (activeTab) {
            activeTab.classList.remove('active');
            activeTab.style.backgroundColor = ''; 
            activeTab.style.color = 'rgba(113, 113, 113, 1)';
        }
        tab.classList.add('active');
        tab.style.color = '#FFF'
        hideContentService();

        const tbName = tab.getAttribute('data-tab');
        const activeContentItem = document.querySelector(`.our_services_nav_content_box[data-tab="${tbName}"]`);
        activeContentItem.style.display = 'block';
        activeTab = tab;
    });
});
    
});

